<?php

namespace App\Controller;

use App\Entity\Quiz;
use App\Entity\User;
use App\Entity\Answer;
use App\Form\QuizType;
use App\Entity\Category;
use App\Entity\Question;
use App\Form\AnswerType;
use App\Form\FilterQuizType;
use App\Repository\QuizRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class QuizController extends AbstractController
{
    /**
     * @Route("/quiz", name="quiz")
     */

    public function index(Quiz $quiz = null, Request $request, ObjectManager $manager, QuizRepository $repo)
    {
        $quizs = $repo->findAll();
        $formFilter = $this->createForm(FilterQuizType::class, $quiz);
        $formFilter->handleRequest($request);

        if ($formFilter->isSubmitted() && $formFilter->isValid())
        {
            $category = $formFilter->get('category')->getData();
            $name = $category->getName();
            $category = $category->getId();
            $number = $formFilter->get('numberOfQuestion')->getData();
            $quizs = $repo->findBy(
                ['category' => $category,
                'numberOfQuestion' => $number],
                ['createdAt' => 'ASC']   
            );
            if (!$quizs)
            {
                return $this->render('quiz/index.html.twig', [
                    'message' => true,
                    'quizs' => $quizs,
                    'formFilterQuiz' => $formFilter->createView()
                ]);  
            }
        }
        
        return $this->render('quiz/index.html.twig', [
            'controller_name' => 'QuizController',
            'message' => false,
            'quizs' => $quizs,
            'formFilterQuiz' => $formFilter->createView()
        ]);
    }

    /**
     * @Route("/", name="home")
     */

    public function home()
    {
        return $this->render('quiz/home.html.twig');
    }

    /**
     * @Route("/quiz/new", name="quiz_create")
     * @Route("/quiz/{id}/edit", name="quiz_edit")
     */

    public function form(Quiz $quiz = null, Request $request, ObjectManager $manager)
    {
        $user = $this->getUser();

        if(!$quiz)
        {
            $quiz = new Quiz();
        }
        
        $form = $this->createForm(QuizType::class, $quiz);
        
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            if(!$quiz->getId())
            {
                $quiz->setCreatedAt(new \DateTime());
                $quiz->setUser($user);
            }
            
            $manager->persist($quiz);
            $manager->flush();

           return $this->redirectToRoute('quiz_questions', ['id' => $quiz->getId()]);
        }

        return $this->render('quiz/create.html.twig', [
            'formQuiz' => $form->createView(),
            'editMode' => $quiz->getId() !== null
        ]);
    }

    /**
     * @Route("/quiz/{id}/question", name="quiz_questions")
     */

    public function newQuestion(Quiz $quiz = null, Request $request, ObjectManager $manager)
    {
        $repos = $this->getDoctrine()->getRepository(Question::class);
        $noq = $quiz->getNumberOfQuestion();
        $questions = $repos->findBy(
            ['quiz' => $quiz]
        );

        if(count($questions) > ($noq - 1))
        {
            return $this->redirectToRoute('quiz_show', ['id' => $quiz->getId()]);
        }

        $question = new Question();

        $form = $this->createFormBuilder($question)
        ->add('content')
        ->getForm();
        
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $question->setQuiz($quiz);
            $manager->persist($question);
            $manager->flush();

            if(count($questions) < $noq)
            {
                return $this->redirectToRoute('quiz_answers', ['id' => $question->getId()]);
            }
            return $this->redirectToRoute('quiz_show', ['id' => $quiz->getId()]);  
        }
        return $this->render('quiz/question.html.twig', [
            'form' => $form->createView()
            ]);
        }
        
        /**
         * @Route("/quiz/{id}/reponse", name="quiz_answers")
         */
        public function newAnswer(Question $question, Request $request, ObjectManager $manager, QuizRepository $repo)
        {   
            $repos = $this->getDoctrine()->getRepository(Answer::class);
            $id_question = $question->getId();
            $quiz = $question->getQuiz();
            $answers = $repos->findBy(
                        ['question' => $id_question]
                    );
            
            $answer = new Answer();
                
            $form = $this->createFormBuilder($answer)
            ->add('content')
            ->add('expectedAnswer')
            ->getForm();
            
            $form->handleRequest($request);
            
            if ($form->isSubmitted() && $form->isValid())
            {
                $answer->setQuestion($question);
                $manager->persist($answer);
                $manager->flush();

                if(count($answers) < 2)
                    {
                        return $this->redirectToRoute('quiz_answers', ['id' => $question->getId()]);
                    }
                
                    return $this->redirectToRoute('quiz_questions', ['id' => $quiz->getId()]);
            }   
        return $this->render('quiz/answer.html.twig', [
                            'formAnswer' => $form->createView()
        ]);
    }

    /**
     * @Route("/quiz/{id}", name="quiz_show")
     */

    public function show(Quiz $quiz = null, Request $request, ObjectManager $manager)
    {
        return $this->render('quiz/show.html.twig', [
            'quiz' => $quiz,
            ]);
    }
}