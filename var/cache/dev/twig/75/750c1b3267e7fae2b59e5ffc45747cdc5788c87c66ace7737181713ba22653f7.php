<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* quiz/index.html.twig */
class __TwigTemplate_81a5ac3db8afc2af2d62cb12aa9ba7e4422f6be43ea6c8757da53024dba21256 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "quiz/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "quiz/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "quiz/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "My_Quizz";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    
    <section class=\"allQuizz d-flex\">
        <aside class=\"category col-3\">
        <h2 class=\"pt-2\">Filtres</h2>
            ";
        // line 10
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["formFilterQuiz"]) || array_key_exists("formFilterQuiz", $context) ? $context["formFilterQuiz"] : (function () { throw new RuntimeError('Variable "formFilterQuiz" does not exist.', 10, $this->source); })()), 'form_start');
        echo "
                ";
        // line 11
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formFilterQuiz"]) || array_key_exists("formFilterQuiz", $context) ? $context["formFilterQuiz"] : (function () { throw new RuntimeError('Variable "formFilterQuiz" does not exist.', 11, $this->source); })()), "category", [], "any", false, false, false, 11), 'row');
        echo "
                ";
        // line 12
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formFilterQuiz"]) || array_key_exists("formFilterQuiz", $context) ? $context["formFilterQuiz"] : (function () { throw new RuntimeError('Variable "formFilterQuiz" does not exist.', 12, $this->source); })()), "numberOfQuestion", [], "any", false, false, false, 12), 'row');
        echo "
                <button type=\"submit\" class=\"btn btn-primary\">Filtrer</button>
            ";
        // line 14
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["formFilterQuiz"]) || array_key_exists("formFilterQuiz", $context) ? $context["formFilterQuiz"] : (function () { throw new RuntimeError('Variable "formFilterQuiz" does not exist.', 14, $this->source); })()), 'form_end');
        echo "
        </aside>
        
        <section class=\"col-9\">
        <h1>Trouvez un quizz</h1>
        
        ";
        // line 20
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["quizs"]) || array_key_exists("quizs", $context) ? $context["quizs"] : (function () { throw new RuntimeError('Variable "quizs" does not exist.', 20, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["quiz"]) {
            // line 21
            echo "        ";
            if (((isset($context["message"]) || array_key_exists("message", $context) ? $context["message"] : (function () { throw new RuntimeError('Variable "message" does not exist.', 21, $this->source); })()) == true)) {
                // line 22
                echo "            Il n'y a pas de quizz dans \" ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["quiz"], "category", [], "any", false, false, false, 22), "name", [], "any", false, false, false, 22), "html", null, true);
                echo " \" en format ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["quiz"], "numberOfQuestion", [], "any", false, false, false, 22), "html", null, true);
                echo " questions!!
        ";
            }
            // line 24
            echo "
            <h2>";
            // line 25
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["quiz"], "name", [], "any", false, false, false, 25), "html", null, true);
            echo "</h2>
            <div class=\"metadata\">Créé le ";
            // line 26
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["quiz"], "createdAt", [], "any", false, false, false, 26), "d/m/Y"), "html", null, true);
            echo " à ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["quiz"], "createdAt", [], "any", false, false, false, 26), "H:i"), "html", null, true);
            echo " par ";
            echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["quiz"], "user", [], "any", false, false, false, 26), "name", [], "any", false, false, false, 26)), "html", null, true);
            echo "
                <h3>Catégorie ";
            // line 27
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["quiz"], "category", [], "any", false, false, false, 27), "name", [], "any", false, false, false, 27), "html", null, true);
            echo "</h3>
                <p>";
            // line 28
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["quiz"], "numberOfQuestion", [], "any", false, false, false, 28), "html", null, true);
            echo " questions.</p>
                <a href=\"";
            // line 29
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("quiz_show", ["id" => twig_get_attribute($this->env, $this->source, $context["quiz"], "id", [], "any", false, false, false, 29)]), "html", null, true);
            echo "\" class=\"btn btn-primary\">Jouez</a>
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['quiz'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 32
        echo "        </section>  
    </section>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "quiz/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  163 => 32,  154 => 29,  150 => 28,  146 => 27,  138 => 26,  134 => 25,  131 => 24,  123 => 22,  120 => 21,  116 => 20,  107 => 14,  102 => 12,  98 => 11,  94 => 10,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}My_Quizz{% endblock %}

{% block body %}
    
    <section class=\"allQuizz d-flex\">
        <aside class=\"category col-3\">
        <h2 class=\"pt-2\">Filtres</h2>
            {{ form_start(formFilterQuiz) }}
                {{ form_row(formFilterQuiz.category) }}
                {{ form_row(formFilterQuiz.numberOfQuestion) }}
                <button type=\"submit\" class=\"btn btn-primary\">Filtrer</button>
            {{ form_end(formFilterQuiz) }}
        </aside>
        
        <section class=\"col-9\">
        <h1>Trouvez un quizz</h1>
        
        {% for quiz in quizs %}
        {% if message == true %}
            Il n'y a pas de quizz dans \" {{ quiz.category.name }} \" en format {{ quiz.numberOfQuestion }} questions!!
        {% endif %}

            <h2>{{ quiz.name }}</h2>
            <div class=\"metadata\">Créé le {{ quiz.createdAt | date('d/m/Y') }} à {{ quiz.createdAt | date('H:i') }} par {{ quiz.user.name | capitalize }}
                <h3>Catégorie {{ quiz.category.name }}</h3>
                <p>{{ quiz.numberOfQuestion }} questions.</p>
                <a href=\"{{ path('quiz_show', {'id': quiz.id}) }}\" class=\"btn btn-primary\">Jouez</a>
            </div>
        {% endfor %}
        </section>  
    </section>
{% endblock %}
", "quiz/index.html.twig", "/home/scopauchan/WAC_1ere/PHP/MVC_My_Quiz/templates/quiz/index.html.twig");
    }
}
