<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* quiz/create.html.twig */
class __TwigTemplate_c8525cb8dad6de3c438ca8d2e364152541ade186ace786ffd5012baaaca540c2 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "quiz/create.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "quiz/create.html.twig"));

        // line 3
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme((isset($context["formQuiz"]) || array_key_exists("formQuiz", $context) ? $context["formQuiz"] : (function () { throw new RuntimeError('Variable "formQuiz" does not exist.', 3, $this->source); })()), [0 => "bootstrap_4_layout.html.twig"], true);
        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "quiz/create.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    ";
        if ((isset($context["editMode"]) || array_key_exists("editMode", $context) ? $context["editMode"] : (function () { throw new RuntimeError('Variable "editMode" does not exist.', 6, $this->source); })())) {
            // line 7
            echo "        <h1>Modifiez un quizz</h1>
    ";
        } else {
            // line 9
            echo "        <h1>Création d'un nouveau quizz</h1>
    ";
        }
        // line 11
        echo "
    ";
        // line 12
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["formQuiz"]) || array_key_exists("formQuiz", $context) ? $context["formQuiz"] : (function () { throw new RuntimeError('Variable "formQuiz" does not exist.', 12, $this->source); })()), 'form_start');
        echo "
    
    ";
        // line 14
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formQuiz"]) || array_key_exists("formQuiz", $context) ? $context["formQuiz"] : (function () { throw new RuntimeError('Variable "formQuiz" does not exist.', 14, $this->source); })()), "name", [], "any", false, false, false, 14), 'row', ["label" => "Nom de votre nouveau Quizz", "attr" => ["placeholder" => "Nom du quizz"]]);
        echo "
    ";
        // line 15
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formQuiz"]) || array_key_exists("formQuiz", $context) ? $context["formQuiz"] : (function () { throw new RuntimeError('Variable "formQuiz" does not exist.', 15, $this->source); })()), "category", [], "any", false, false, false, 15), 'row', ["label" => "Choisissez une catégorie"]);
        echo "
    ";
        // line 16
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formQuiz"]) || array_key_exists("formQuiz", $context) ? $context["formQuiz"] : (function () { throw new RuntimeError('Variable "formQuiz" does not exist.', 16, $this->source); })()), "numberOfQuestion", [], "any", false, false, false, 16), 'row', ["label" => "Choisissez une catégorie"]);
        echo "

    <button type=\"submit\" class=\"btn btn-primary\">
        ";
        // line 19
        if ((isset($context["editMode"]) || array_key_exists("editMode", $context) ? $context["editMode"] : (function () { throw new RuntimeError('Variable "editMode" does not exist.', 19, $this->source); })())) {
            // line 20
            echo "            Modifier
        ";
        } else {
            // line 22
            echo "            Créer un nouveau quizz    
        ";
        }
        // line 24
        echo "    </button>
        
    ";
        // line 26
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["formQuiz"]) || array_key_exists("formQuiz", $context) ? $context["formQuiz"] : (function () { throw new RuntimeError('Variable "formQuiz" does not exist.', 26, $this->source); })()), 'form_end');
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "quiz/create.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  118 => 26,  114 => 24,  110 => 22,  106 => 20,  104 => 19,  98 => 16,  94 => 15,  90 => 14,  85 => 12,  82 => 11,  78 => 9,  74 => 7,  71 => 6,  61 => 5,  50 => 1,  48 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% form_theme formQuiz 'bootstrap_4_layout.html.twig' %}

{% block body %}
    {% if editMode %}
        <h1>Modifiez un quizz</h1>
    {% else %}
        <h1>Création d'un nouveau quizz</h1>
    {% endif %}

    {{ form_start(formQuiz) }}
    
    {{ form_row(formQuiz.name, {'label': 'Nom de votre nouveau Quizz', 'attr': {'placeholder': 'Nom du quizz'}}) }}
    {{ form_row(formQuiz.category, {'label': 'Choisissez une catégorie'}) }}
    {{ form_row(formQuiz.numberOfQuestion, {'label': 'Choisissez une catégorie'}) }}

    <button type=\"submit\" class=\"btn btn-primary\">
        {% if editMode %}
            Modifier
        {% else %}
            Créer un nouveau quizz    
        {% endif %}
    </button>
        
    {{ form_end(formQuiz) }}
{% endblock %}", "quiz/create.html.twig", "/home/scopauchan/WAC_1ere/PHP/MVC_My_Quiz/templates/quiz/create.html.twig");
    }
}
